# Examen Segunda Evaluación Diseño Web y Desarrollo Cliente

# Día 10/03/2022 Tiempo: 5 horas

- Nota: Cada pregunta se valorará como bien o como mal (valoraciones intermedias serán excepcionales).
- Nota2: En cada pregunta se especifica si se valora en el examen de diseño o en el de desarrollo.
- Nota3: Para aprobar cada examen hay que obtener una puntuación mínima de 5 puntos en ese examen.
- Nota4: Organice su tiempo. Si no consigue resolver un apartado pase al siguiente. El examen consta de apartados de diseño y de desarrollo que se pueden resolver por separado. Si un apartado depende de otro que no sabe resolver, siempre puede dar una solución que aunque no sea correcta, le permita seguir avanzando.
- Nota5: Para que una solución sea correcta, no sólo hay que conseguir que haga lo que se pide, sino que además **todo lo que funcionaba lo tiene que seguir haciendo**. La solución debe estar implementada según las prácticas de código limpio explicadas en clase. Esto incluye JavaScript, CSS y HTML.
- Nota6: Lea completamente el examen antes de empezar y comience por lo que le parezca más fácil.
- Nota7: No se permite utilizar ninguna librería que no esté ya incluida en el fichero "package.json".
- Nota8: Debe implementar la solución en los ficheros que se le proporcionan.
- Nota9: No debe haber "warnings" ni errores en la consola.

Pasos previos antes de empezar

- Clone el repositorio del enunciado

```bash
git clone https://gitlab.com/surtich/enunciado-examen-final-2021-2022.git enunciado-examen-final
```

- Vaya al directorio del proyecto

```bash
cd enunciado-examen-segunda
```

- Configure su usuario de git:

```bash
git config user.name "Sustituya por su nombre y apellidos"
git config user.email "Sustituya por su correo electrónico"
```

- Cree un _branch_ con su nombre y apellidos separados con guiones (no incluya mayúsculas, acentos o caracteres no alfabéticos, excepción hecha de los guiones). Ejemplo:

```bash
    git checkout -b fulanito-perez-gomez
```

- Compruebe que está en la rama correcta:

```bash
    git status
```

- Suba la rama al repositorio remoto:

```bash
    git push origin <nombre-de-la-rama-dado-anteriormente>
```

- Arrange el servidor:

```bash
    yarn server
```

- Compruebe que suministra datos [http://localhost:3002/books](http://localhost:3002/books):

- En otro terminal, instale las dependencias y arranque el cliente:

```bash
    yarn install
    yarn start
```

Navegue a [http://localhost:3000](http://localhost:3000)


- Dígale al profesor que ya ha terminado para que compruebe que todo es correcto y desconecte la red.

## OBJETIVO

Se trata de implementar la funcionalidad de búsqueda.

![Video 0](./img/0.gif)


### 1.- Caja de búsqueda de la pantalla de descubrimiento.

El resultado final será.

![Image 1](./img/1.png)

#### 1.1.- (1 punto, diseño) La barra de navegación tendrá el estilo mostrado.

Nota: Elimine el outline de la caja de texto y del botón. Fíjese en tamaños y márgenes.

#### 1.2.- (0.5 puntos, desarrollo) Durante la búsqueda se mostrará un `Spinner` (ya implementado). Al finalizar la búsqueda en vez del `Spinner` se mostrará:

```html
<p>Here you go! Find more books with the search bar above.</p>
```

![Video 1.2](./img/1.2.gif)

#### 1.3.- (0.5 puntos, desarrollo) Durante la búsqueda se mostrará un segundo `spinner` en la caja de búsqueda.

![Video 1.3](./img/1.3.gif)


#### 1.4.- (1 punto, diseño) Los textos estarán centrados.

![Image 1.4](./img/1.4.png)


#### 1.5.- (1 punto, desarrollo) Si introduce un texto en la caja de búsqueda y pulsa sobre la lupa, se llamará a la función `load_books` pasándole el término de búsqueda y se mostrarán los libros que devuelva esta función.

Nota: Se mantendrá el comportamiento de que la primera búsqueda se haga automáticamente al navegar a descubrimiento.

#### 1.6.- (1 punto, desarrollo) Cuando se busque a través de la caja de búsqueda, no se mostrará el `spinner` grande (sí el de la caja de búsqueda) y en lugar de los libros de la búsqueda anterior, se mostrará un `placeholder`.

Nota: La imagen del `placeholder` es el fichero `loading-book-placeholder.svg`

![Video 1.6](./img/1.6.gif)


#### 1.7.- (0.5 puntos, desarrollo) Tras la primera búsqueda hecha en la caja de búsqueda, se ocultarán los textos de la pantalla de descubrimiento.

![Image 1.7](./img/1.6.png)

#### 1.8.- (1 punto, desarrollo) Si después de realizar una búsqueda navega a la pantalla de lectura o de fin y luego vuelve a descubrimiento, se mantendrá la búsqueda realizada.

![Video 1.8](./img/1.8.gif)

#### 1.9.- (1 punto, desarrollo) Si se pulsa sobre la lupa sin cambiar el término de búsqueda, no se realizará una nueva búsqueda.

![Video 1.9](./img/1.9.gif)

#### 1.10.- (0.5 puntos, desarrollo) Si al realizar la búsqueda no se encuentra ningún libro, se mostrará:

```html
<p>
    Hmmm... I couldn't find any books with the query "{searchTerm}"
    Please try another.
</p>
```

![Video 1.10](./img/1.10.gif)

#### 1.11.- (0.5 puntos, desarrollo) Suponga que ha hecho una búsqueda y no ha encontrado libros, entonces no tiene sentido en la siguiente búsqueda hacer lo que se pide en `1.6`. En esta situación, al realizar una nueva búsqueda, se volverá a mostrar el `spinner`.

Nota: Para puntuar en este apartado es imprescindible que lo haya hecho también en el apartado 1.6.

![Video 1.11](./img/1.11.gif)

### 2.- Modificaciones en las listas.

Al implementar la búsqueda tenemos un problema debido a que sólo hay una lista de libros: Suponga que añade un libro de la lista de descubrimiento a la de lectura y que luego cambia la búsqueda de tal forma que el libro añadido a lectura ya no es uno de los buscados. Si vuelve a lectura, el libro ya no estará allí ya que no forma parte de la búsqueda. Compruebe que ocurre esto.

La solución para este apartado va a consistir en tener tres listas de libros: una de descubrimiento, otra de lectura y otra de libros finalizados.

#### 2.1.- (1 punto, desarrollo) En la pantalla de descubrimiento, al pulsar sobre `+` el libro se mantendrá en la lista de descubrimiento y se añadirá a la lista de lectura, mostrándose en la pantalla de lectura.

![Video 2.1](./img/2.1.gif)


#### 2.2.- (0.5 puntos, desarrollo) Si pulsa `check` en la lista de lectura, el libro se quitará de esta lista y se pasará a la de finalizados y si pulsa `-` simplemente se quitará de la lista de lectura.


#### 2.3.- (0.5 puntos, desarrollo) Si pulsa `read` en la lista de finalizados, el libro se quitará de esta lista y se pasará a la de lectura y si pulsa `-` simplemente se quitará de la lista de finalizados.

#### 2.4.- (1 punto, desarrollo) En la lista de descubrimiento, si un libro ya está en la lista de lectura o de finalizados, se ocultará el botón `+` (para que no se pueda volver a añadir) y se mostrará en qué lista está.


#### 2.5.- (1 punto, diseño) El libro se verá con la opacidad mostrada.

![Image 2.5](./img/2.5.png)


#### 2.6.- (1 punto, diseño) El texto tendrá el estilo mostrado y estará centrado.

![Image 2.6](./img/2.6.png)

### 3.- Comportamiento responsivo en el punto de interrupción.

![Image 3](./img/3.png)

#### 3.1.- (1 punto, diseño) En el punto de interrupción sólo se mostrará la imagen de cada libro.
#### 3.2.- (2 puntos, diseño) Los libros tendrán la apariencia mostrada.

Nota: Tenga en cuenta alineación, tamaño y márgenes.

#### 3.3.- (2 puntos, diseño) Hay libros que tienen imágenes más pequeñas y más grandes que otras. Todos los libros tendrán el mismo tamaño independientemente del tamaño de su imagen. La imagen estará centrada y no distorsionada.

#### 3.4.- (1 punto, diseño) Los botones estarán bien colocados incluso aunque haya pocos libros.

Nota: Se ha comprobado que con el código suministrado los botones se descolocan en la vista de lectura y de finalizados.

![Image 3.4](./img/3.4.png)

#### 3.5.- (0.5 puntos, desarrollo) Los textos `Reading` y `Finished` no caben en la vista móvil, sustitúyalos por `R` y `F` respectivamente.

Pista: se suministra el siguiente código que permite guardar en un estado el tamaño actual de la ventana.

```javascript
useEffect(function () {
    setWindowWidth(window.innerWidth);
    function onResize() {
      setWindowWidth(window.innerWidth);
    }
    window.addEventListener("resize", onResize);
    return function () {
      window.removeEventListener("resize", onResize);
    };
  }, []);
```

## Para entregar

- Ejecute el siguiente comando para comprobar que está en la rama correcta y ver los ficheros que ha cambiado:

```bash
    git status
```

- Prepare los cambios para que se añadan al repositorio local:

```bash
    git add .
    git commit -m "completed exam"
```

- Compruebe que no tiene más cambios que incluir:

```bash
    git status
```

- Dígale al profesor que va a entregar el examen.

- Conecte la red y ejecute el siguiente comando:

```bash
    git push origin <nombre-de-la-rama>
```

- Abandone el aula en silencio.