const initialBooksState = {
  loading: false,
  books: [],
  readingBooks: [],
  finishedBooks: [],
};

function addBook(list, book) {
  return [...list, book];
}

function removeBook(list, book) {
  return list.filter((b) => b.id !== book.id);
}

const bookReducer = (state, action) => {
  switch (action.type) {
    case "BOOK_FETCH_INIT":
      return {
        ...state,
        loading: true,
      };
    case "BOOK_FETCH_SUCCESS":
      return {
        ...state,
        loading: false,
        books: action.payload.books,
      };
    case "BOOK_ADD_READING":
      return {
        ...state,
        readingBooks: action.payload.reading
          ? addBook(state.readingBooks, action.payload.book)
          : removeBook(state.readingBooks, action.payload.book),
        finishedBooks: removeBook(state.finishedBooks, action.payload.book),
      };
    case "BOOK_ADD_FINISHED":
      return {
        ...state,
        finishedBooks: addBook(state.finishedBooks, action.payload.book),
        readingBooks: removeBook(state.readingBooks, action.payload.book),
      };
    default:
      throw Error("Action Books Error!");
  }
};

export { initialBooksState, bookReducer };
