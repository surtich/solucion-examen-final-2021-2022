import "./Nav.scss";

export default function Nav({ navOption, setNavOption }) {
  return (
    <div>
      <nav className="nav">
        <ul className="nav__ul">
          <li>
            <a
              className={`nav__link ${
                navOption === 0 ? "nav__link--selected" : ""
              }`}
              onClick={(e) => {
                e.nativeEvent.preventDefault();
                setNavOption(0);
              }}
              href="/"
            >
              Reading List
            </a>
          </li>
          <li>
            <a
              className={`nav__link ${
                navOption === 1 ? "nav__link--selected" : ""
              }`}
              onClick={(e) => {
                e.nativeEvent.preventDefault();
                setNavOption(1);
              }}
              href="/"
            >
              Finished Books
            </a>
          </li>
          <li>
            <a
              className={`nav__link ${
                navOption === 2 ? "nav__link--selected" : ""
              }`}
              onClick={(e) => {
                e.nativeEvent.preventDefault();
                setNavOption(2);
              }}
              href="/"
            >
              Discover
            </a>
          </li>
        </ul>
      </nav>
    </div>
  );
}
