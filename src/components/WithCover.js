function WithCover({ text, children }) {
  if (!text) {
    return children;
  }
  return (
    <div
      style={{
        zIndex: " 1",
        position: "relative",
      }}
    >
      <div
        style={{
          position: "absolute",
          top: "50%",
          left: "50%",
          transform: "translate(-50%, -50%)",
          fontSize: "10rem",
        }}
      >
        {text}
      </div>
      {children}
    </div>
  );
}

export default WithCover;
