import Search from "./Search";
import { Spinner } from "./Spinner";

export default function Text({
  navOption,
  loading,
  gotoDiscover,
  searchBooks,
  searchTerm,
  setSearchTerm,
  books,
  searchTimes,
}) {
  const showText =
    (navOption !== 2 && books.length === 0) ||
    (navOption === 2 && searchTimes < 2);

  const failSearch = navOption === 2 && books.length === 0 && searchTimes > 1;

  const text = (t) => (
    <p>
      Hey there! {t} Get started by heading over to{" "}
      <a
        onClick={(e) => {
          e.nativeEvent.preventDefault();
          gotoDiscover();
        }}
        href="/"
      >
        the Discover page
      </a>{" "}
      to add books to your list.
    </p>
  );
  switch (navOption) {
    case 0:
      return showText && text("Welcome to your bookshelf reading list.");
    case 1:
      return (
        showText &&
        text("This is where books will go when you've finished reading them.")
      );
    case 2:
    default:
      return (
        <div
          style={{
            textAlign: "center",
          }}
        >
          <Search
            loading={loading}
            searchBooks={searchBooks}
            searchTerm={searchTerm}
            setSearchTerm={setSearchTerm}
          />
          {showText && (
            <>
              <p>Welcome to the discover page.</p>
              <p>Here, let me load a few books for you...</p>
            </>
          )}

          {loading ? (
            books.length === 0 && (
              <Spinner
                style={{
                  fontSize: "3rem",
                  marginBottom: "1rem",
                }}
              />
            )
          ) : failSearch ? (
            <p>
              Hmmm... I couldn't find any books with the query "{searchTerm}"
              Please try another.
            </p>
          ) : (
            showText && (
              <p>Here you go! Find more books with the search bar above.</p>
            )
          )}
        </div>
      );
  }
}
