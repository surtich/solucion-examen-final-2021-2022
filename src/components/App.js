import { useCallback, useEffect, useReducer, useState } from "react";
import Nav from "./Nav";
import "./App.scss";
import { bookReducer, initialBooksState } from "../reducers/book";
import { API_ENDPOINT, loadBooks } from "../api/books";
import Book from "./Book";
import Text from "./Text";
import loadingBookPlaceholder from "./loading-book-placeholder.svg";
import WithCover from "./WithCover";

const loadingBook = {
  title: "Loading...",
  author: "loading...",
  coverImageUrl: loadingBookPlaceholder,
  publisher: "Loading Publishing",
  synopsis: "Loading...",
  loadingBook: true,
};

export default function App() {
  const [windowWidth, setWindowWidth] = useState(0);
  const [navOption, setNavOption] = useState(0);
  const [bookState, bookDispatch] = useReducer(bookReducer, initialBooksState);
  const [searchTimes, setSearchTimes] = useState(0);
  const [searchTerm, setSearchTerm] = useState("");

  const searchBooks = useCallback((searchTerm = "") => {
    setSearchTerm(searchTerm);
    bookDispatch({
      type: "BOOK_FETCH_INIT",
    });
    loadBooks(searchTerm).then(function ({ books }) {
      setSearchTimes((searchTimes) => searchTimes + 1);
      bookDispatch({
        type: "BOOK_FETCH_SUCCESS",
        payload: {
          books,
        },
      });
    });
  }, []);

  useEffect(
    function () {
      if (navOption === 2 && searchTimes === 0) {
        searchBooks();
      }
    },
    [navOption, searchTimes, searchBooks]
  );

  useEffect(function () {
    setWindowWidth(window.innerWidth);
    function onResize() {
      setWindowWidth(window.innerWidth);
    }
    window.addEventListener("resize", onResize);
    return function () {
      window.removeEventListener("resize", onResize);
    };
  }, []);

  function addToReading(book, reading = true) {
    return function () {
      bookDispatch({
        type: "BOOK_ADD_READING",
        payload: {
          book,
          reading,
        },
      });
    };
  }

  function addToFinished(book) {
    return function () {
      bookDispatch({
        type: "BOOK_ADD_FINISHED",
        payload: {
          book,
        },
      });
    };
  }

  function showBooks() {
    switch (navOption) {
      case 0:
        return bookState.readingBooks.map((book) => {
          const coverImageUrl = `${API_ENDPOINT}/${book.coverImageUrl}`;
          return (
            <li key={book.id} style={{ marginBottom: "1rem" }}>
              <Book
                {...book}
                coverImageUrl={coverImageUrl}
                onCheck={addToFinished(book)}
                onMinus={addToReading(book, false)}
              />
            </li>
          );
        });
      case 1:
        return bookState.finishedBooks.map((book) => {
          const coverImageUrl = `${API_ENDPOINT}/${book.coverImageUrl}`;
          return (
            <li key={book.id} style={{ marginBottom: "1rem" }}>
              <Book
                {...book}
                coverImageUrl={coverImageUrl}
                onRead={addToReading(book, true)}
                onMinus={addToReading(book, false)}
              />
            </li>
          );
        });
      case 2:
        return bookState.books.map((book) => {
          const coverImageUrl = `${API_ENDPOINT}/${book.coverImageUrl}`;
          const inReadingList = bookState.readingBooks.find(
            (b) => b.id === book.id
          );
          const inFinishedList = bookState.finishedBooks.find(
            (b) => b.id === book.id
          );
          const coverText = inReadingList
            ? windowWidth > 991
              ? "Reading"
              : "R"
            : inFinishedList
            ? windowWidth > 991
              ? "Finished"
              : "F"
            : "";

          return (
            <li key={book.id} style={{ marginBottom: "1rem" }}>
              {bookState.loading ? (
                <Book {...loadingBook} onPlus={() => {}} />
              ) : (
                <WithCover text={coverText}>
                  <Book
                    {...book}
                    coverImageUrl={coverImageUrl}
                    coverText={coverText}
                    onPlus={addToReading(book)}
                  />
                </WithCover>
              )}
            </li>
          );
        });
      default:
        return [];
    }
  }

  const books = showBooks();

  return (
    <div className="app">
      <Nav navOption={navOption} setNavOption={setNavOption} />
      <div className="body">
        <Text
          navOption={navOption}
          loading={bookState.loading}
          gotoDiscover={() => setNavOption(2)}
          searchBooks={searchBooks}
          searchTerm={searchTerm}
          setSearchTerm={setSearchTerm}
          books={books}
          searchTimes={searchTimes}
        />
        <ul className="book-list">{books}</ul>
      </div>
    </div>
  );
}
