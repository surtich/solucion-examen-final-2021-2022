import { useState } from "react";
import { FaSearch } from "react-icons/fa";
import "./Search.scss";
import { Spinner } from "./Spinner";

function Search({ loading, searchBooks, searchTerm }) {
  const [lastSearch, setLastSearch] = useState(searchTerm);
  function handleClick(e) {
    e.preventDefault();
    if (lastSearch.toLowerCase() !== searchTerm.toLowerCase()) {
      searchBooks(lastSearch);
    }
  }
  return (
    <div className="search">
      <form>
        <input
          placeholder="Search books..."
          id="search"
          type="text"
          value={lastSearch}
          onChange={(e) => setLastSearch(e.target.value)}
        />
        <label htmlFor="search">
          <button type="submit" onClick={handleClick}>
            {loading ? <Spinner /> : <FaSearch />}
          </button>
        </label>
      </form>
    </div>
  );
}

export default Search;
